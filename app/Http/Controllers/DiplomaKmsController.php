<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DiplomaKmsController extends Controller
{
    // Token de autenticação gerado no BRy Cloud
    const token = "TOKEN_BRY_CLOUD";
    // URL da API de assinatura BRy
    const urlAssinatura = "https://diploma.hom.bry.com.br/api/xml-signature-service/v1/signatures/kms";
    #const urlAssinatura = "https://diploma.bry.com.br/api/xml-signature-service/v1/signatures/kms";

    public function assinaXMLDiplomado(Request $request)
    {
        // Cria array de dados que serão enviados na requisição
        $dados = array(
            'nonce' => '1',
            'signatureFormat ' => 'ENVELOPED',
            'hashAlgorithm' => 'SHA256',
            'originalDocuments[0][nonce]' => '1',
        );

        //Gera kms_data e adiciona na request
        $kms_type = $request->kms_type;
        $kms_credencial = $request->valor_credencial;
        $kms_data = "{";
        if (strcasecmp($kms_type, 'BRYKMS') == 0) {
            // Caso seja BRyKMS
            // Adiciona Credencial de acesso (PIN ou TOKEN)
            if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'TOKEN') == 0) {
                $kms_data = $kms_data . '"token" : "' . $kms_credencial . '"';
            } else {
                $kms_data = $kms_data . '"pin" : "' . $kms_credencial . '"';
            }
            // Se parâmetros de identificação foram enviados, os adiciona
            if ($request->uuid_cert) {
                $kms_data = $kms_data . ',"uuid_cert" : "' . $request->uuid_cert . '"';
            } else if ($request->user) {
                $kms_data = $kms_data . ',"user" : "' . $request->user . '"';
            }
        } else if (strcasecmp($kms_type, 'DINAMO') == 0) {
            // Caso seja DINAMO
            // Adiciona Credencial de acesso (Pin, OTP ou Token)
            if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'TOKEN') == 0) {
                $kms_data = $kms_data . '"token" : "' . $kms_credencial . '"';
            } else if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'OTP') == 0){
                $kms_data = $kms_data . '"otp" : "' . $kms_credencial . '"';
            } else {
                $kms_data = $kms_data . '"pin" : "' . $kms_credencial . '"';
            }

            //Se usuário for enviado adiciona. Utilizado somente junto a PIN
            if ($request->user) {
                $kms_data = $kms_data . ',"user" : "' . $request->user . '"';
            }
            
            // Adiciona parâmetros para identificação de certificado e chave privada
            $kms_data = $kms_data . ',"uuid_cert" : "' . $request->uuid_cert . '"';
            $kms_data = $kms_data . ',"uuid_pkey" : "' . $request->uuid_pkey . '"';
        } else {
            return response(array('message' => 'KMS_TYPE Invalido', 400));
        }
        $kms_data = $kms_data . '}';
        $dados['kms_data'] = $kms_data;

        //Se recebeu um arquivo na request, adiciona o arquivo. Senão adiciona o XML armazenado
        $tipoRetorno = "";
        if (is_null($request->documento) || $request->documento == "null" || $request->documento == "") {
            $tipoRetorno = "BASE64";

            $dados['originalDocuments[0][content]'] = new \CURLFILE(storage_path() . '/XMLsOriginais/exemplo-xml-diploma-digital.xml');
            $dados['returnType'] = $tipoRetorno;
        } else {
            $tipoRetorno = "LINK";

            $dados['originalDocuments[0][content]'] = new \CURLFILE($request->documento);
            $dados['returnType'] = $tipoRetorno;
        }

        // Cria uma requisição CURL diferente para cada tipo de Assinante
        switch ($request->tipoAssinatura) {
                // Assinatura do tipo Representantes
            case 'Representantes':
                $dados['profile'] = 'ADRC';
                #Pode ser necessário alterar este parâmetro para DadosRegistroNSF de acordo com o arquivo sendo utilizado.
                $dados['originalDocuments[0][specificNode][name]'] = 'DadosRegistro';
                $dados['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd';
                break;

                // Assinatura do tipo IES Registradora
            case 'IESRegistradora':
                $dados['profile'] = 'ADRA';
                $dados['includeXPathEnveloped'] = 'false';
                break;
            default:
                return '{"message" : "Erro ao identificar o tipo de assinante em assinatura de Diploma."}';
        }

        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => self::urlAssinatura,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $dados,
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . self::token,
                'kms_type: ' .  $kms_type
            ),
        ));

        // Envia a requisição para a API de Assinatura BRy
        $resposta = curl_exec($curl);
        // Recebe o código HTTP da requisição
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON
        $respostaJson = json_decode($resposta);
        // Encerra a requisição
        curl_close($curl);

        // Se a requisição retornou o código HTTP 200, ou seja, sucesso
        //      , ele retorna para o front a requisição, se foi enviado um arquivo ou uma mensagem, se foi utilizado o arquivo local.
        // Caso não tenha retornado 200, retorna a resposta vindo da API.
        if ($httpcode == 200) {
            if ($tipoRetorno == 'BASE64') {
                $arquivoAssinado = fopen(storage_path() . '/XMLsAssinados/exemplo-diploma-assinado.xml', 'wb');
                fwrite($arquivoAssinado,  base64_decode($respostaJson[0]));
                return '{"message" : "Arquivo salvo localmente."}';
            } else {
                return response()->json($respostaJson);
            }

        } else {
            return response()->json($respostaJson);
        }
    }

    public function assinaXMLDocumentacao(Request $request)
    {
        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        // Cria array de dados que serão enviados na requisição
        $dados = array(
            'nonce' => '1',
            'signatureFormat ' => 'ENVELOPED',
            'hashAlgorithm' => 'SHA256',
            'originalDocuments[0][nonce]' => '1',
        );

        //Gera kms_data e adiciona na request
        $kms_type = $request->kms_type;
        $kms_credencial = $request->valor_credencial;
        $kms_data = "{";
        if (strcasecmp($kms_type, 'BRYKMS') == 0) {
            // Caso seja BRyKMS
            // Adiciona Credencial de acesso (PIN ou TOKEN)
            if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'TOKEN') == 0) {
                $kms_data = $kms_data . '"token" : "' . $kms_credencial . '"';
            } else {
                $kms_data = $kms_data . '"pin" : "' . $kms_credencial . '"';
            }
            // Se parâmetros de identificação foram enviados, os adiciona
            if ($request->uuid_cert) {
                $kms_data = $kms_data . ',"uuid_cert" : "' . $request->uuid_cert . '"';
            } else if ($request->user) {
                $kms_data = $kms_data . ',"user" : "' . $request->user . '"';
            }
        } else if (strcasecmp($kms_type, 'DINAMO') == 0) {
            // Caso seja DINAMO
            // Adiciona Credencial de acesso (Pin, OTP ou Token)
            if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'TOKEN') == 0) {
                $kms_data = $kms_data . '"token" : "' . $kms_credencial . '"';
            } else if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'OTP') == 0){
                $kms_data = $kms_data . '"otp" : "' . $kms_credencial . '"';
            } else {
                $kms_data = $kms_data . '"pin" : "' . $kms_credencial . '"';
            }
        
            //Se usuário for enviado adiciona. Utilizado somente junto a PIN
            if ($request->user) {
                $kms_data = $kms_data . ',"user" : "' . $request->user . '"';
            }
                    
            // Adiciona parâmetros para identificação de certificado e chave privada
            $kms_data = $kms_data . ',"uuid_cert" : "' . $request->uuid_cert . '"';
            $kms_data = $kms_data . ',"uuid_pkey" : "' . $request->uuid_pkey . '"';
        } else {
            return response(array('message' => 'KMS_TYPE Invalido', 400));
        }
        $kms_data = $kms_data . '}';
        $dados['kms_data'] = $kms_data;

        //Se recebeu um arquivo na request, adiciona o arquivo. Senão adiciona o XML armazenado
        $tipoRetorno = "";
        if (is_null($request->documento) || $request->documento == "null" || $request->documento == "") {
            $tipoRetorno = "BASE64";

            $dados['originalDocuments[0][content]'] = new \CURLFILE(storage_path() . '/XMLsOriginais/exemplo-xml-diploma-digital.xml');
            $dados['returnType'] = $tipoRetorno;
        } else {
            $tipoRetorno = "LINK";

            $dados['originalDocuments[0][content]'] = new \CURLFILE($request->documento);
            $dados['returnType'] = $tipoRetorno;
        }

        // Cria uma requisição CURL diferente para cada tipo de Assinante
        switch ($request->tipoAssinatura) {
            // Assinatura do tipo IES Representantes
            case 'Representantes':
                $dados['profile'] = 'ADRC';
                #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
                $dados['originalDocuments[0][specificNode][name]'] = 'DadosDiploma';
                $dados['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd';
                break;
            // Assinatura do tipo IES Emissora no Nodo Dados Diploma
            case 'IESEmissoraDadosDiploma':
                $dados['profile'] = 'ADRC';
                #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
                $dados['originalDocuments[0][specificNode][name]'] = 'DadosDiploma';
                $dados['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd';
                $dados['includeXPathEnveloped'] = 'false';
                break;
            case 'IESEmissoraRegistro':
                $dados['profile'] = 'ADRA';
                $dados['includeXPathEnveloped'] = 'false';
                break;
            default:
                return '{"message" : "Erro ao identificar o tipo de assinante em assinatura de Documentação Academica."}';
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::urlAssinatura,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $dados,
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . self::token,
                'kms_type: ' .  $kms_type,
            ),
        ));

        // Envia a requisição para a API de Assinatura BRy
        $resposta = curl_exec($curl);
        // Recebe o código HTTP da requisição
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON
        $respostaJson = json_decode($resposta);
        // Encerra a requisição
        curl_close($curl);
        error_log($httpcode);
 
        // Se a requisição retornou o código HTTP 200, ou seja, sucesso
        //      , ele retorna para o front a requisição, se foi enviado um arquivo ou uma mensagem, se foi utilizado o arquivo local.
        // Caso não tenha retornado 200, retorna a resposta vindo da API.
        if ($httpcode == 200) {
            if ($tipoRetorno == 'BASE64') {
                $arquivoAssinado = fopen(storage_path() . '/XMLsAssinados/exemplo-documentacao-academica-assinada.xml', 'wb');
                fwrite($arquivoAssinado,  base64_decode($respostaJson[0]));
                return '{"message" : "Arquivo salvo localmente."}';
            } else {
                return response()->json($respostaJson);
            }

        } else {
            return response()->json($respostaJson);
        }
    }

    public function copiarNodo(Request $request) 
    {
        return '{"message":"Operação não suportada pelo back-end. Realize a copia do nodo manuelmente ou utilize os exemplos em Python ou Java."}';
    }
}
